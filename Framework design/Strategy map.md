### Strategy map
The Strategy Maps give an insight on four perspectives:

##### 1. Mission
The mission in this case is the request of a standard SBR stakeholder that wants to use ML.
It is derived from the perspective that the SBR stakeholders strive to operational excellence.
Because the need for operational excellence, the SBR stakeholders want to improve their service, reduce administrational workload and reduce costs.

##### 2. Customers/ beneficiaries
The customer and beneficiaries value proposition is at the core of the strategy, which is why it comes directly after mission.
The value propositions revolve around serving the needs of the beneficiaries. In the case of the “general” SBR customers:
- Costs
- Quality
- Functionality
- Service

##### 3. Internal Processes
In this specific case the main focus is to improve internal processes by using Machine Learning.

##### 4. Learning and growth
Learning and growth is the foundation of the strategy.
This perspective outlines the employee skills and knowledge required to make the processes run smoothly, resulting in added value to customers.

##### Final strategy map
These 4 perspectives combined result in the following strategy map:

<img src="Resources/Strategy%20map.png" alt="Strategy map" width="600"/>
