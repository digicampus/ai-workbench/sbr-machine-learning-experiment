## Method Description
This section elaborates on the designed method. It first provides a description on how and in which situation to use the method. Secondly, it elaborates on what the method can do for you. Last, it provides a detailed description of each step in the method.

**When can the method be used**  
This method is created for organizations that have SBR-like structured data. Therefore one needs to ask if the data is relatable to the data SBR provides.  Second, the method provides the necessary process steps to achieve a machine learning model. It is important to realize that this step-by-step plan can be carried out by someone with minimal Machine Learning experience, but also by an expert. In the first case, the method provides more insight into the added value of ML, whereas the latter, with an expert, it also becomes a reliable and operational model.  


**What can the method provide**  
What the method actually provides can be divided into two parts. First of all, the method gives the user a general insight into what is involved in setting up a machine learning project. It also helps the user to make a reliable estimate for which cases machine can be used. These aspects give the potential user a proper idea of what it takes to set it up a model and if Machine Learning has added value within this context. This process step is extremely relevant to prevent initiating a machine learning project without a sound estimate of the applicability. What the method provides, before actually starting a Machine Learning Project, is:  

-	It gives policy makers and engineers an overview of what it takes to start a Machine Learning project (including preconditions and restrictions)  
-	It provides insight into and examples of, possible applications of Machine Learning  
-	It enables a structured process for both engineers and managers; creating alignment and understanding of management and engineers can lead to synergy  
-	It presents guidelines to implement the project ethically  

When employing the method, the user endeavours into a structured and partly iterative process to set up machine learning projects. The user is guided step by step through the process of a machine learning project. Taking into account organizational and ethical aspects as part of an ML project.
When all steps are completed, the process delivers the following deliverables:  
-	insight into whether Machine Learning has added value for the organization  
-	a machine learning model (and or insights)  
    -  produced ethically  
-	insights into what the future organizational need might be  e.g. in human recourses, more employees with Machine Learning expertise, fewer employees with "old skills"  

The Machine Learning Project Method method is constructed in a way that both policy makers and employees understand which steps have to be taken to develop a successful machine learning project. This method consists of three main elements. Besides finding the right machine learning algorithm, this method takes into account the organizational context and ethics. 

This can be visualized with the following flowchart:

<img src="Resources/machine_learning_project_method.png" alt="Machine Learning Project Method" width="1200"/>

First, a [Strategy map](Strategy%20map.md) is developed, which helps to identify four different perspectives on the proposed strategy.  

The following section discribes the method step by step.  

**Step 1: Goal Formulation**

**Step 1.1: Check context method**  
The method is originally designed for specific users and for a specific goal. Before using this method, it is important to check if this method really fits the proposed use. As explained, this method provides clear guidelines in setting up a machine learning project considering ethical, organisational and machine learning factors. The method provides guidelines on how to setup a machine learning project and provides a prototype machine learning model. The designed method is only tested on for a specified user (SBR-stakeholders) working with a specified data structure (SBR structured data).
Example/output: Check if the method fits with the context of the user  

**Step 1.2 Set up project goal**  
Formulating and defining the goal of the project. It emphasizes the importance of a clear project goal formulation.  
Source: (Fayyad et al., 1996; Han et al., 2011), Experiment-DUO  
Example/output: estimating the probability if an organization is fraudulent.  

**Step 1.3 Does the goal fit with machine learning?** 
Analyzation of the goal is needed to determine whether it is compatible with machine learning. If the goal is not compatible with machine learning the goal must be reformulated or there might be other data mining options that are more appropriate. Where examples for machine learning forms are:   

The following figure shows the Machine Learning techniques:

<img src="Resources/machine_learning_techniques.png" alt="Machine Learning Techniques" width="800"/>  

- Association & clustering are there to gain insights into the data.  
    E.g. clustering the data into groups to define new groups.  
- Classification is there to predict a categorial value  
    E.g. to predict the label financial risk which can be the label yes or no  
- Regression focusses on predicting a numeric value  
    E.g. to predict the amount of subsidies per organization  

- Example/Output: the estimation the of governmental subsidy which is numeric value corresponds with supervised machine learning, specifically, regression.  


**Step 2: Project Team Setup**
In order to create a successful project one should consider the knowledge that is needed (Fayyad et al., 1996). Kaplan & Norton (2004) explain that to apply a new strategy a certain learning and growth needs to be analysed. In other words, certain knowledge and skills are needed and or need to be acquired. T

**Step 2.1: Include internal stakeholders**  
Management and employees together are relevant internal stakeholders combining all kinds of relevant knowledge. It also supports the potential impact on the work-processes by the management in line with the strategy of the organization. Furthermore, this contributes to the internal communication with the relevant internal stakeholders, so that all can benefit from the project and provide additional input.
Example: Include internal stakeholders and communicate intern

**Step 2.2: Include person that has:**  
The following skills are required in the project team (Han et al., 2011):  
- Machine learning experience  
- Data experience  
- Data access  
- Domain knowledge  
- Statistical experience  

Source: (Fayyad et al., 1996; Han et al., 2011)  
Example: Add people who have in multiple knowledge fields (step 2.2.1-2.2.5) relevant  experience  
  
 
**Step 3: Context Analysis**  
After setting up the goal and the project team, certain context needs to be analysed. First, a stakeholder analysis will be done providing clear understanding on what the stakeholders want and what their values are (Davis & Nathan, 2015; Kaplan & Norton, 2004). Insight of the ethical factors and impacts must be part of this analysis. Second, the current situation of project goal has been analysed in order to record a clear t=0, which in the final steps support the understanding and evaluating of the project. The last action is to conduct an Ethical Impact Assessment. There is a need for ethical guidance during a machine learning project. One of the actions that helps to execute an ethical project is to do an Ethical Impact Assessment (Reijers et al., 2016; Wright, 2011). The framework of doing an Ethical Impact Assessment will be provided with the method.  

**Step 3.1: Conduct a stakeholder analysis**  
A stakeholder analysis is done in order to identify the relevant stakeholders, to understand what is important for the stakeholders (their expectations & their values) and to analyse how the stakeholders would be affected by completing a machine learning project.  
Source: (Kaplan & Norton, 2004) Beneficiary’s perspective, (Davis & Nathan, 2015; Wright, 2011)  
Example/output: Full stakeholder analysis on the stakeholders affected by the DUO financial inspection  

**Step 3.2: Analyse current situation of goal**  
Mapping of the current situation of the selected goal is important  to compare the current/original/t=0 setting with the outcome of the machine learning experiment. This helps to evaluate the overall project on the added value of machine learning.  
Source: (Kaplan & Norton, 2004), (Wright, 2011), Experiment-DUO  
Example/output: At this moment fraud detection in banking cost X and is done by Y hours of human calculation  

**Step 3.3: Start Ethical Impact Assessment**  
The ethical impact analyses will be done in order the clarify  the potential ethical effects before conducting a machine learning experiment.  
Source: (Reijers et al., 2016; Wright, 2011)  
Example/output: a complete Ethical Impact Analysis on the use of machine learning to detect fraud in care allowance  
  
  
**Step 4: Data Collection**  
The next step focused on the relevant data that needed to create a working and reliable machine learning model. This step is relatively straightforward but can be time consuming. However, as the SBR data is stored properly and therefore relatively easy to access (Bharosa et al., 2015). Conducting the data collection has to be done by someone that has experience with data and access to the data, described in step 2 as it is important to select the appropriate data, garbage in, garbage out (Han et al., 2011).  

**Step 4.1: gather (structured) data **  
Select and gather the appropriate data.  
Source: (Fayyad et al., 1996; Han et al., 2011)  
Example: Retract the financial data from the “DUO” server  

**Step 4.2 Apply feature engineering**  
This step perfectly aligns with the need for that the team member with the domain experience is the same person that should conduct the feature engineering. Furthermore, Han et al. (2011) describe the process as data transformation, confirming the relevance for this step.  
Source: (Fayyad et al., 1996; Han et al., 2011)  
Example: Combine two attributes to one attribute that has a higher predicting power  
  
  
**Step 5: Data Preparation**  
The data preparation step is done to create a dataset which can be used to train the machine learning models. First, the quality of the data is checked. This helps to indicate which actions need to be performed. Second, data preparation steps described by (Fayyad et al., 1996; Han et al., 2011) include: cleaning the data, removing noise and handling missing data. Third, the transformation of the dataset to a format which is readable by a Machine Learning Workbench. As part of this research, Weka has been used. The last action is to split the dataset into two sets, a training set and a test set. This is done so that the produced model, which is built on the training data, can be evaluated on unseen data. Which indicates the effectiveness of the model on unseen data and check if the model was not overfit on the training dataset (Han et al., 2011).

**Step 5.1: Check data quality**  
Analyse the quality of the data to indicate if there is a need to clean and or to restructure the data.  
Source: (Fayyad et al., 1996; Han et al., 2011)  
Example/output: Analyse if there is data missing to be able to understand what needs to be done regarding data cleaning actions  

**Step 5.2: Clean the data**  
In this step the results of the previous steps are analysed so that the data cleaning tasks can be performed as there are; combine, reduce, replace and or remove parts.  
Source: (Fayyad et al., 1996; Han et al., 2011)  
Example/output: Remove attributes in cases of too many missing instances for a clean dataset  

**Step 5.3: Make the data Machine Learning Workbench (Weka) readable**  
This steps adjusts the data format as it needs to be changed into a format readable by the machine learning workbench, in this case, Weka.  
Source: (Hall et al., 2009), Experiment-DUO  
Example/output: Change to format from XBRL to CSV to ARFF to have a data format which can be used for building machine learning models  

**Step 5.4: Split the data**  
This steps indicates the importance of splitting the dataset into a training dataset and into a test dataset. This step is indispensable as the model can be checked on unseen data and therefore checked on overfitting. (optional: Split in three sets, add evaluation set)  
Source: (Fayyad et al., 1996; Han et al., 2011), Experiment-DUO  
Example/output: The DUO dataset is split into a dataset including data from 2015-2018 and a dataset including the data from 2019  
  
  
**Step 6: Algorithm Selection**
This step requires the user of the method to select the algorithms from the algorithm selection method.

The algorithm selection method provides the user clear guidelines on which algorithm to use the achieve based on machine learning goal determent in Step 1.2 . Furthermore, it provides algorithms which are relatively “easy” to understand (Fayyad et al., 1996; Han et al., 2011). This helps projects with no expert knowledge of machine learning understanding the considerations the model takes. Additionally, choosing an algorithm which is relatively “easy” to understand provides the ability to explain the model to stakeholders which contributes to the transparency of the project. And transparency of the project diminished the ethical problems (Davis & Nathan, 2015).  

**Step 6.1: select the machine learning algorithms from the algorithm selection method**  
Select the relevant algorithms that fit with the machine learning goal.  
Source: (Brownlee, 2016; Davis & Nathan, 2015; Han et al., 2011; Keskinbora, 2019; Qiang & Xindong, 2006; Raab, 2020), Experiment-DUO  
Example/output: The machine learning goal is a classification problem.  Therefore we use the following algorithms: Logistic Regression, Naive Bayes ,k-Nearest Neighbours , Decision Trees, Support Vector Machines  

The following figure shows the algorithm selection method made for SBR stakeholders:

<img src="Resources/machine_learning_selection_method.png" alt="Algorithm_Selection_Method" width="1000"/>
  
**Step 7: Model Building**  
Based on the algorithms selected in the previous step, this step uses these algorithms to build the machine learning models. Hereafter, the created models are tested on the unseen test data. Lastly, the results are noted and analysed (Fayyad et al., 1996; Han et al., 2011).

**Step 7.1: Use the algorithms on the dataset to create the models**  
This step is to train the model. A crucial step within this process of machine learning. This is done by using the algorithms selected in the previous step and training them on the prepared dataset (step 5, data preparation). For this step the standard settings of the algorithms are used (Hall et al., 2009).  
Source: (Brownlee, 2016; Fayyad et al., 1996; Hall et al., 2009), Experiments-DUO  
Example/output: Use the algorithms from Step 6.1 on the dataset to train the models  

**Step 7.2: Test the models on the test dataset**  
Testing the dataset on the test data helps to indicate if the model is built in a way that it is not prone to overfitting. This avoid bias and produces a “fair” model.  
Source: (Fayyad et al., 1996; Han et al., 2011), Experiment-DUO  
Example/output: Test the built model on the unseen dataset of 2018  

**Step 7.3: Note the results**  
Note/record the results of the experiment so that they can be compared.  
Source: Experiment DUO  
Example/output: See the example xxx.  

**Step 7.4: Compare the results**  
This step is created to evaluate the results of the models built in the previous step. These results need to be interpreted and compared so that the most promising model can be selected. To be able to understand the results on has to learn on how to interpret the results.
Source: (Fayyad et al., 1996; Hall et al., 2009; Han et al., 2011), Experiment-DUO  
Example/output: Interpreting the accuracy and precision of the models and comparing them with each other  
  
  
**Step 8: Model Adjusting**  
The next step is to adjust the parameters of the selected model and/or use ensemble machine learning to examine if it is possible to improve the results. Nonetheless, this step is not always necessary as the original defined goal can already be achieved. Yet, it might be possible to create a model which is superior than the defined goal.  

**Step 8.1: Select the best model**  
This steps indicates the selection of the best model for further improving of the model in line with the research question. In this step it is important to select the results of the model most important/ promising for the project.  
Source: (Bishop, 2006; Han et al., 2011)  
Example/output: from the models that are built model X has highest accuracy and the best precision. Therefor model X is chosen.  

**Step 8.2: Systematically change parameters**  
In order to improve the model it might be possible to systematically change the parameters of the algorithm. The results can be noted and compared. And the risk of overfitting should be taken into account.  
Source: (Bishop, 2006; Hall et al., 2009), Experiment-DUO  
Example/output: turning the parameter “NoPruning” in a decision tree algorithm provided an increase in accuracy  

**Step 8.3: Use ensemble machine learning**  
Ensemble machine learning is found to be effective in increasing the results of algorithms in some specific cases (Han et al., 2011, p. 377). Therefore, this steps stipulates the option to select an ensemble machine learning technique to improve the created model.  
Source: (Han et al., 2011, p. 377; Wu et al., 2008), Experiment-DUO  
Example/output: Use ADAbooster to discover if it is possible to improve the model  
  
  
**Step 9: Project Evaluation**  
After the previous step, the new model needs to be tested on the previous unseen dataset. Furthermore, it needs to be decided if the machine learning project has been successful. This will be done by comparing the final results to the original situation, which is analysed in Step 3.2. Lastly, it needs to be determined if there is a need for additional machine learning expertise to further develop and improve the model.  

**Step 1: Check results model / test the results on the test dataset**  
This step focusses on evaluating the improvements that are made in the previous step. This is done by testing the dataset on the unseen test dataset. Furthermore, the results of the model are evaluated.
Source: (Han et al., 2011, p. 21), Experiment-DUO  
Example/output: The improved model is tested on the unseen test data set of 2018  

**Step 2: Compare results with current situation**  
This step focusses on evaluating and comparing the model on the situation it was produced for.
Source: (Fayyad et al., 1996; Han et al., 2011), Experiment-DUO  
Example/output: Before a system was able to predict fraud with an accuracy of 90%, the new model is able to predict fraud with an accuracy of 95%  

**Step 9.3: Evaluate the need of a machine learning expert**  
This steps implies the evaluating whether it has an added value tot including a machine learning expert on the project. The experiment can be conducted by someone with minimal machine learning experience, but to further improve the model, there might be a need for a machine learning expert.
Source: Experiment-DUO  
Example/output: Because the project was done with minimal machine learning experience and the model shows protentional, a machine learning expert is advised.  
  
  
**Step 10: Communication**  
This final step describes  the communication of the project. Although the project has been implemented in closes interaction with the internal stakeholders, the management and employees, a final and transparent communication by the project members is crucial. It supports the potential impact on the work-processes by the management in line with the strategy of the organization.  

As stipulated by Umbrello & Bellis (2018) & Wright (2011) it is important for projects like these, they are thoroughly communicated with the internal and external stakeholders. This contributes to the transparency and reproducibility of the project.  

**Step 10.1: Communicate results with management**  
Communicating results with management keeps the alignment with employee and employers so that the work provides information to the decision makers and is not redundant.  
Source: (Fayyad et al., 1996; Kaplan & Norton, 2004)  
Example: Setup a meeting with management to explain the final results so that informed decision making can be conducted.  

**Step 10.2: Document the project**  
The overall project should be documented internally. By documentation the project internally it helps other employees in the organization to have easily access to the finding of the project. Furthermore, if the employees who participated in the project are no longer working for the company, the project is clearly documented.  
Example: Transcribe and store all the relevant deliverables in the cloud of the organization  

**Step 10.3: Share on GitLab (optional)**  
Most interviewees mentioned this step as important; however it isoptional as not all organizations’ can share their data, e.g. because of privacy reasons. However, by sharing the project and its results the organization creates transparency, which is preferred by Friedman et al. (2013) & Wright (2011).  
Source: (Davis & Nathan, 2015; Friedman et al., 2013)  
Example: Share the project on GitLab to provide insight on how the model was built  

**Sources**  
Bharosa, N., van Wijk, R., & de Winne, N. (2015). Challenging the Chain: Governing the Automated Exchange and Processing of Business Information. Ios Press.  
Bishop, C. M. (2006). Pattern recognition and machine learning. Springer.  
Brownlee, J. (2016). Machine learning mastery with Weka. Ebook. Edition: V. 1.4.  
Davis, J., & Nathan, L. (2015). Value Sensitive Design: Applications, Adaptations, and Critiques. Handbook of Ethics, Values, and Technological Design: Sources, Theory, Values and Application Domains, 11–40. https://doi.org/10.1007/978-94-007-6970-0_3  
Fayyad, U., Piatetsky-Shapiro, G., & Smyth, P. (1996). From Data Mining to Knowledge Discovery in Databases. AI Magazine, 17(3), 37–37. https://doi.org/10.1609/aimag.v17i3.1230  
Friedman, B., Kahn, P. H., Borning, A., & Huldtgren, A. (2013). Value Sensitive Design and Information Systems. In N. Doorn, D. Schuurbiers, I. van de Poel, & M. E. Gorman (Eds.), Early engagement and new technologies: Opening up the laboratory (pp. 55–95). Springer Netherlands. https://doi.org/10.1007/978-94-007-7844-3_4  
Hall, M., Frank, E., Holmes, G., Pfahringer, B., Reutemann, P., & Witten, I. H. (2009). The WEKA data mining software: An update. ACM SIGKDD Explorations Newsletter, 11(1), 10–18. https://doi.org/10.1145/1656274.1656278  
Han, J., Pei, J., & Kamber, M. (2011). Data Mining: Concepts and Techniques. https://books.google.nl/books?hl=nl&lr=&id=pQws07tdpjoC&oi=fnd&pg=PP1&dq=data+mining+concepts+and+techniques&ots=tzLtYVkCZX&sig=KZFU16u7Rzt4VL9QpbjGgQywrMk&redir_esc=y#v=onepage&q=data%20mining%20concepts%20and%20techniques&f=false  
Kaplan, R. S., & Norton, D. P. (2004). The strategy map: Guide to aligning intangible assets. Strategy & Leadership.  
Keskinbora, K. H. (2019). Medical ethics considerations on artificial intelligence. Journal of Clinical Neuroscience, 64, 277–282. Scopus. https://doi.org/10.1016/j.jocn.2019.03.001  
Qiang, Y., & Xindong, W. (2006). 10 Challenging problems in data mining research. International Journal of Information Technology and Decision Making, 5(4), 597–604. Scopus. https://doi.org/10.1142/S0219622006002258  
Raab, C. D. (2020). Information privacy, impact assessment, and the place of ethics. Computer Law and Security Review. Scopus. https://doi.org/10.1016/j.clsr.2020.105404  
Reijers, W., Brey, P., Jansen, P., Rodriques, R., Wright, D., Koivisto, R., Tuominen, A., & Bitsch, L. (2016). A Common Framework for Ethical Impact Assessment. European Commission EC. https://satoriproject.eu/media/D4.1_Annex_1_EIA_Proposal.pdf  
Wright, D. (2011). A framework for the ethical impact assessment of information technology. Ethics and Information Technology, 13(3), 199–226. https://doi.org/10.1007/s10676-010-9242-6  
Wu, X., Kumar, V., Ross Quinlan, J., Ghosh, J., Yang, Q., Motoda, H., McLachlan, G. J., Ng, A., Liu, B., Yu, P. S., Zhou, Z.-H., Steinbach, M., Hand, D. J., & Steinberg, D. (2008). Top 10 algorithms in data mining. Knowledge and Information Systems, 14(1), 1–37. https://doi.org/10.1007/s10115-007-0114-2  




