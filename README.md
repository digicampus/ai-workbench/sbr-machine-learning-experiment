# SBR Machine Learning Experiment


This repo contains:
[Experiments](Experiments) done with data from DUO and SBR-Wonen/WSW.  
A method to conduct your own experiment can be found here: [Method](Framework_Design).  

A detailed description of the goal of this project is discribed in the following section.  

**Objective**  
The main objective of this project is to help actors in Standard Business Reporting (SBR) chains to systematically assess set up Machine Learning projects so that they can analyse the potential value of using Machine Learning (ML) on their structured data.
Therefore, a method that combines both technical, ethical and organizational aspects has been developed. This method supports SBR-stakeholders to systematically set up machine learning projects. 
This helps the actors in SBR chains to systematically assess the potential value of using machine learning in combination with organizational and ethical factors on their structured data. 
Furthermore, it will support the stakeholders to assess if the proposed machine learning project could be viable so that the stakeholders have a higher chance of developing a successful machine learning project.

To build the method we applied and tested ML algorithms on XBRL datasets:
1. [Classification](Experiments/1.%20Classification)
2. [Regression](Experiments/2.%20Regression)

A detailed description of these steps - as well as the used XBRL data - can be found in the [Experiments](Experiments) section.  

These experiments, together with literature en interviews with experts, form the basis of the designed method.  

A simplified representation is visualized in the following flow diagram.  

<img src="Resources/Simplified_version_designed_method.png" alt="Machine Learning Project method Simplified" width="1000"/>

Next the designed method is demonstrated on a case of SBR-Wonen/WSW. The demonstration case will be elaborated in Demonstration however, this section is still under construction [Demonstration](XXXX).

If you want to conduct your own experiment, you can follow the methodology laid out in the ML [Machine Learning Project Method](Framework%20design).
Furthermore, we recommend to read the demonstration as it provides a clear example.





## License

The content in this repository is licensed under Creative Commons v4, Attribution-ShareAlike. See the [LICENSE file](./LICENSE) or the [online summary](https://creativecommons.org/licenses/by-sa/4.0/) for details.
