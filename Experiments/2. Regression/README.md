### Experiment 2: regression

This experiment is executed to answer the second hypothesis:  
*With a regression model it is possible to predict the governmental subsidy with a Root Relative Square Error lower than 25%.*


#### 1. Data collection & 2. Data preparation
For this experiment, the same dataset is used for the first experiment. Therefore, the first step and the second step are the same as explained in the first experiment. There is only one adjustment which is changing the class that needs to be analysed to the governmental subsidy attribute. In order to train a Support Vector Machine model, the algorithm cannot handle date attributes. Therefore, for training the SVM model the date attribute is deleted. experiment it will be the attribute sector.

#### 3. Model selection
The objective is to predict a numeric attribute. This requires a regression algorithm. Five algorithms are selected from the Table 2 that fit the regression problem the best. These five algorithms for a regression problem are:
•	Linear Regression  
•	k-Nearest Neighbours  
•	Decision Trees  
•	Support Vector Machines   
•	Artificial Neural Network  


#### 4. Model training
The algorithms are trained on the training dataset with the standard settings of Weka (Hall et al., 2009). 
The results of the models are displayed the Table below.

###### 4.1 Algorithms:
Algorithm | Algorithm Weka | RMSE | RRSE(%) |
--- | --- | --- | ---
Logistic Regression | functions.LinearRegression | 13155867| 31.6686
k-Nearest Neighbors | lazy.IBk | 113668168| 273.6199
Decision Trees | Trees.REPtree | 11170476| 26.8911
Support Vector Machines | functions.SMOreg | 17881820| 43.0448
Artificial Neural Network | functions.MultilayerPerceptron | 12218352| 29.4118

#### 4.2 Parameter tuning
In order to examine if it is possible to create an improved model, the parameters are systematically changed.The results of changing the parameters are described in the Table below.

|     Setting                      |     Standard setting    |     New setting    |     Decrease RMSE %    |
|----------------------------------|-------------------------|--------------------|------------------------|
|     Batch size                   |     100                 |     100            |     0%                 |
|     Debug                        |     False               |     False          |     0%                 |
|     doNotCheckCapabilities       |     False               |     False          |     0%                 |
|     initial count                |     0.0                 |     1.0 / 2.0      |     0 %                |
|     MaxDepth                     |     -1                  |     -1             |     0%                 |
|     minNum                       |     2.0                 |     1.0            |     1,4356%            |
|     minVarianceProp              |     0.001               |     0.001          |     0%                 |
|     noPruning                    |     False               |     True           |     5.1742%            |
|     numDecimalPlaces             |     2                   |     2              |     0%                 |
|     numFolds                     |     3                   |     5              |     0.2284%            |
|     seed                         |     1                   |     3              |     2.6278%            |
|     SpreadInitialCount           |     False               |     False          |     0%                 |

#### 4.3 Boosting
In order to examine the option to create a better model, the possibilities of ensemble machine learning are tried. Using Bagging with the standard Weka parameters (Hall et al., 2009) are:
RMSE = 8810534.9278 and RRSE = 21.2099 %.

Systematically changing the parameters of Bagging produced improvement when the parameter was set to noPruning. This model achieved the best results which are a Root mean squared error of 8390352 and Root relative squared error of 20.1984 %. The model was tested on the training data and no significant change was found.


#### 5. Evaluation
Consequent following the steps for creating a machine learning project, finally produced a model which achieves an RRSE of 20.1984%. This result is lower than the RRSE from the hypothesis 2: “With a regression model it is possible to predict the governmental subsidy with a Root Relative Square Error lower than 25%”. Therefore this hypothesis is confirmed.



