#### Boosting
In machine learning, boosting is an ensemble meta-algorithm for primarily reducing bias, and also variance in supervised learning,
and a family of machine learning algorithms that convert weak learners to strong ones.
Boosting is based on the question posed by Kearns and Valiant (1988) "Can a set of weak learners create a single strong learner?"
A weak learner is defined to be a classifier that is only slightly correlated with the true classification (it can label examples better than random guessing).
In contrast, a strong learner is a classifier that is arbitrarily well-correlated with the true classification.

A promising booster that can be used is AdaBoost, short for Adaptive Boosting (Wu et al., 2008).
It can be used in conjunction with many other types of learning algorithms to improve performance.
The output of the other learning algorithms ('weak learners') is combined into a weighted sum that represents the final output of the boosted classifier.
AdaBoost is adaptive in the sense that subsequent weak learners are tweaked in favour of those instances misclassified by previous classifiers.

Every learning algorithm tends to suit some problem types better than others,
and typically has many different parameters and configurations to adjust before it achieves optimal performance on a dataset,
AdaBoost (with decision trees as the weak learners) is often referred to as the best out-of-the-box classifier.
When used with decision tree learning, information gathered at each stage of the AdaBoost algorithm about the relative 'hardness' of each
training sample is fed into the tree growing algorithm such that later trees tend to focus on harder-to-classify examples.

The ADAbooster settings are (Witten & Frank, 2002):
- Seed: The random number seed to be used.
- weightThreshold:Weight threshold for weight pruning.
- numDecimalPlaces: The number of decimal places to be used for the output of numbers in the model.
- batchSize: The preferred number of instances to process if batch prediction is being performed. More or fewer instances may be provided, but this gives implementations a chance to specify a preferred batch size.
- numIterations: The number of iterations to be performed.
- Debug: If set to true, classifier may output additional info to the console.
- Resume: Set whether classifier can continue training after performing the requested number of iterations.
- Classifier: The base classifier to be used.
- doNotCheckCapabilities: If set, classifier capabilities are not checked before classifier is built
- useResampling : Whether resampling is used instead of reweighting.

The Adabooster can be found in Weka under the Meta settings.
Using the Adabooster with standard settings, the accuracy is increased  with 2.3633% to 89.1981%.
Table .. shows all the REPTree settings which were systematically changed in the Adabooster.

Setting | Standard setting | New setting | Increase %
--- | --- | --- | ---
Batch size  | 100 | 100 | 0%
Debug no change | False | False | 0%
doNotCheckCapabilities  | False | False | 0%
initial count  | 0.0 | 0.0 | 0.%
MaxDepth  | -1 | -1 | 0%
minNUm  | 2.0 | 1.0 | 0.0758%
minVarianceProp  | 0.001 | 0.001 | 0%
noPruning  | False | True | 1.1968%
numDecimalPlaces  | 2 | 2 | 0%
numFolds | 3 | 8 | 0.3787%
seed | 1 | 1 | 0%
SpreadInitialCount | False | False | 0%

With the ADAbooster and the REPtree setting of noPruning to True, an accuracy of 90.3952% was achieved.
Now ADAbooster also has options that can be changed.
These settings are also explored and the results are shown in table ..

Setting | Standard setting | New setting | Increase %
--- | --- | --- | ---
Batch size no change | 100 | 100 | 0%
Debug  | False | False | 0%
doNotCheckCapabilities  | False | False | 0%
numDecimalPlaces | 2.0 | 2.0 | 0%
numIterations | -1 | 100 | 1.9391%
Resume | False | False | 0%
seed | 1 | -1 | 0.0879%
useResampling | False | True | 0.6060%
weightthreshold | 100 | 100 | 0%

Changing all the settings of the Adabooster systemically resulted in an increase of accuracy,
with the most promising setting, changing numIterations led to an accuracy of 91.1377%.

The next step is to try to combine multiple options.
However, this process is described more as an artform rather than an exact science (Brownlee, 2016).

During the experiments we noticed that with pruning and numiterations we reach a maximum of 91.062.
These two settings are the most promising from both algorithms.
However, we also noticed that if we turn off pruning, we reach a higher maximum.
This shows that even with the maximum settings “off”, for example the REPtree, it doesn’t mean that in de ADAbooster you should use this settings.
A combination of other settings might be higher. Therefore, all combinations of the settings that provided a positive effect of the accuracy are combined in different setups.
The most promising combination is when the ADAbooster adapted settings are numIteration is on 100 and resampling is true and the REPtree are the standard settings.
This combination of settings has led to an accuracy of 91.2438%.
This is increase of 4.4085% from the original REPtree settings that led to an accuracy of 86.8353%.

After finalization of the model, it was used to predict the classes from the unseen data from 2018.
This resulted in an accuracy of 91.6061%.
The hypothesis was: With a classification algorithm it is possible to predict the correct label of the DUO XBRL dataset with +90% accuracy.
This hypothesis was succeeded. Therefore, the hypothesis was true.

Risk Overfitting

TODO: spread all this info over several files?
