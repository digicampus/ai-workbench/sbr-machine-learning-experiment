### Experiment 1: classification

This experiment is executed to answer the first hypothesis:  
*With a classification algorithm it is possible to predict the correct label of the DUO XBRL dataset with +90% accuracy.*

#### Setup
- Installed Weka
- Installed the WekaExcell addon to open csv format in Weka
- Installed the LibSVM to produce Vectors analysis

#### 1. Data collection
- Downloaded the sets from DUO
- Add “Governmental Subsidies” and “Total result” which are the cost & benefits
- Changed the csv format to an ARFF, because Weka uses the ARFF format
- Changed the “date” from a numerical attribute to a date attribute
- Deleted the Authorised supervision, Grouping, Name to make the instances not relate to each other
- Split the dataset into the year 2014-2017 and 2018 which are subsequently the training dataset and the validation dataset

#### 2. Data preparation
- Remove the attributes that have more then 85% missing files which are:
  - Immateriele vaste active 92% missing
  - Voorraden 89% missing
  - Kortlopende effecten 90% missing
- In order to make an attribute the class that needs to analysed, the attribute needs to changed into a class attribute in Weka. For the first experiment it will be the attribute sector.

#### 3. Model selection
- The objective is to predict a nominal label. This requires a classifying algorithm.
  - For classifying the Sector attribute, we use the top 5 algorithms of Weka to see which Algorithm has the highest predicting power.
  - We use cross validation 10 folds the most common/best learning settings

#### 4. Model training
- The model will be trained on the dataset from 2014-2017.
- For the training the standard settings will be used. A detailed description of the settings and results can be found in Appendix ….

###### 4.1 Algorithms:
Algorithm | Algorithm Weka | Correctly Classified Instances (%)
--- | --- | ---
Logistic Regression | functions.Logistic | 84.6993%
Naive Bayes | NaiveBayesMultinomialText | 70.8226 %
k-Nearest Neighbors | lazy.IBk | 18.8002 %
Classification and Regression Trees | trees.REPTree | 86.8353 %
Support Vector Machines | functions.LibSVM | 70.8075 %

#### 4.2 Parameter tuning
The REP.Tree algorithm preformed the best.
This is the starting point of further tuning the algorithm to explore if it is possible to create a higher accuracy.
Several settings could be changed, and each of these was explored as described [here](Parameter%20tuning.md).

#### 4.3 Boosting
To reach a higher accuracy, the possibilities of boosting are explored, which is described [here](Boosting.md).

#### 5. Evaluate model
The evaluation on the unseen dataset dit not provide significat changes.
Consequent following the steps for creating a machine learning project, finally produced a model which achieves an RRSE of 20.1984%. This result is lower than the RRSE from the hypothesis 2: “With a regression model it is possible to predict the governmental subsidy with a Root Relative Square Error lower than 25%”. Therefore this hypothesis is confirmed.

