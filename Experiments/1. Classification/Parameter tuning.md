#### 6. Parameter tuning
The following settings can be changed (Witten & Frank, 2002):
- Seed: The seed used for randomizing the data.
- minNum: The minimum total weight of the instances in a leaf.
- numFolds: Determines the amount of data used for pruning. One fold is used for pruning, the rest for growing the rules.
- numDecimalPlaces: The number of decimal places to be used for the output of numbers in the model.
- batchSize: The preferred number of instances to process if batch prediction is being performed. More or fewer instances may be provided, but this gives implementations a chance to specify a preferred batch size.
- Debug: If set to true, classifier may output additional info to the console.
- noPruning: Whether pruning is performed.
- spreadInitialCount: Spread initial count across all values instead of using the count per value.
- doNotCheckCapabilities: If set, classifier capabilities are not checked before classifier is built (Use with caution to reduce runtime).
- maxDepth: The maximum tree depth (-1 for no restriction).
- minVarianceProp: The minimum proportion of the variance on all the data that needs to be present at a node in order for splitting to be performed in regression trees.
- initialCount: Initial class value count.

The settings named above were systematically explored. These results - including if changing the settings led to an increase - are described in the following below:

Setting | Standard setting | New setting | Increase %
--- | --- | --- | ---
Batch size | 100 | 100 | 0%
Debug | False | False | 0%
doNotCheckCapabilities | False | False | 0%
initial count | 0.0 | 1.0 / 2.0 | 0.0303%
MaxDepth | -1 | -1 | 0%
minNUm | 2.0 | 1.0 | 0.0606%
minVarianceProp | 0.001 | 0.001 | 0%
noPruning | False | True | 0.2424%
numDecimalPlaces | 2 | 2 | 0%
numFolds | 3 | 4 | 0.1364%
seed | 1 | -2 | 0.4242%
SpreadInitialCount | False | False | 0%
>This table shows the increases per setting.
The next step is to combine the settings and explore if it reaches a higher accuracy.  
The combination were tried and did not lead to an higher result. The next step is to use a Booster.  
Furthermore, we recognize that changing the seed in not the right way to adjust the model, as it leads to overfitting.  
However, we tried every setting in Weka just to understand all the outcomes.  
