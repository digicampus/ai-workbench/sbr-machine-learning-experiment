## Experiments to create the method
In this GitLab we describe two kind of exeriments. The experiments that are done to create the method and the experiments that done following the method.
This section elaborated on the experiments to create the method. The experiments in this repository describe the implementation of Machine learning on the DUO dataset.
The following hypotheses are set, each will eventually include an associated experiment:
1. With a [classification](1.%20Classification) algorithm it is possible to predict the correct label of the DUO XBRL dataset with +90% accuracy.
2. With a [regression](2.%20Regression) algorithm it is possible to predict the government subsidy per educational institution from the DUO XBRL dataset with an RSME of 25% accuracy.

### Experiment steps:
Each of these experiments are executed using the following steps:

**1. Data collection:**  
  Collect the data that is needed.
  The outcome of this step is generally a representation of data (Guo simplifies to specifying a table) which we will use for training.

**2. Data preparation:**  
  Cleaning the data, the removal of noise, and the handling of missing data.

**3. Algorithm selection:**  
  Select the proper algorithms using the algorithms selection method.

**4. Model training:**   
  Use the algorithms to create models.
  The goal of training is to answer a question or make a prediction correctly as often as possible.
  Linear regression example: algorithm would need to learn values for m (or W) and b (x is input, y is output).
  Each iteration of process is a training step.
  This step refers to hyperparameter tuning, which is an "artform" as opposed to a science.
  Tune model parameters for improved performance.
  
**5. Evaluate model:**   
  Evaluating the machine learning model, patterns and project
  Uses some metric or combination of metrics to "measure" objective performance of model.
  Test the model against previously unseen data.



### Data
Each experiment was performed on the DUO [datasets](Data) included in this repository, which include:
- A dataset used to train the model in both [xlsx](Data/DUO%202014-2017.xlsx) and [ARFF](Data/DUO%202014-2017.xlsx.arff) format
- A dataset used to test the model ([xlsx](Data/DUO%202018.xlsx.arff) - [ARFF](Data/DUO%202018.xlsx.arff))
